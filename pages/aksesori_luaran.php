<?php session_start(); ?>

<?php if(!defined("URL")) define("URL", "/aksesori_kereta/"); ?>

<?php include "../pages/template/header.php"; ?>

<?php include "../pages/template/top-bar.php"; ?>

<?php include "../pages/template/left-content.php";  ?>

<?php include("../config.php"); ?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<!DOCTYPE html>
<html>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Hubungi Kami</h2>
            </div>
            <!-- Exportable Table -->
            <?php $results = mysqli_query($con, "SELECT * FROM aks_luaran"); ?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                EXPORTABLE TABLE
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>ID </th>
                                            <th>NAMA AKSESORI DALAMAN</th>
                                            <th>HARGA</th>
                                            <th>JENAMA</th>
                                            <th>WARNA</th>
                                            <th>KATEGORI</th>

                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID </th>
                                            <th>NAMA AKSESORI DALAMAN</th>
                                            <th>HARGA</th>
                                            <th>JENAMA</th>
                                            <th>WARNA</th>
                                            <th>KATEGORI</th>

                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php while ($row = mysqli_fetch_array($results)) { ?>
                                        <tr>
                                            <td><?php echo $row['id_aks_luaran']; ?></td>
                                            <td><?php echo $row['nama_aks_luaran']; ?></td>
                                            <td><?php echo $row['harga_aks_luaran']; ?></td>
                                            <td><?php echo $row['jenama_aks_luaran']; ?></td>
                                            <td><?php echo $row['warna_aks_luaran']; ?></td>
                                            <td><?php echo $row['kategori_aks_luaran']; ?></td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
    </section>
    <?php include "../pages/template/footer.php"; ?>
</body>

</html>
