<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages//header_print.php";           ?>

<?php
$table = 'ketua_keluarga';
$id = $_GET['id'];
//$no_kp_from = $id;
$i = 1;
//$_SESSION['test'] = $id;

$query = "SELECT * FROM ketua_keluarga WHERE no_kp ='$id'";
    $result = mysqli_query($connect, $query);
    $count = mysqli_num_rows($result);

    $row=mysqli_fetch_array($result);

$query2 = "SELECT * FROM isteri_waris WHERE no_kp_ketua ='$id'";
    $result2 = mysqli_query($connect, $query2);
    $count2 = mysqli_num_rows($result2);

    

$query3 = "SELECT * FROM tanggungan WHERE no_kp_ketua ='$id'";
    $result3 = mysqli_query($connect, $query3);
    $count3 = mysqli_num_rows($result3);

?>
      
      
<div class="row">        
<div style="margin-top=20px; margin-left:120px; margin-right:120px;">        
<div id="div1">
    
        <!--Table Gambar dan Maklumat Johor-->
        <table align="center" width="100%">
         <tr>
           <td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 115.5px; height: 115.5px;"></td>
			<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
			<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
			</td>
			<td width="20%"></td>
          </tr>
        </table><br>
        <!--End Table Gambar dan maklumat Johor-->
        
       <div>
	   <!--Table Ketua Keluarga-->
       <b><i>Maklumat Peribadi Ketua Keluarga:</i></b><br>
        <table align="center" frame="" width="100%">
		<tr class="spaceUnder">
			<td width="17%">Nama</td>
            <td width="1%">:&nbsp;</td>
			<td width="31%"><?php echo $row['nama'] ?></td>
			<td width="20%">No K/P</td> 
			<td width="1%">:&nbsp;</td>
			<td width="28%"><?php echo $row['no_kp'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tarikh Lahir</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row['tarikh_lahir'] ?></td>
			<td>Jantina</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row['jantina'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tempat Kelahiran</td>
			<td>:&nbsp;</td>
			<td><?php echo $row['tempat_lahir'] ?></td>
			<td>Keturunan/ Bangsa</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row['keturunan'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Taraf Perkahwinan</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row['taraf_kahwin'] ?></td>
			<td>Agama</td><br>
			<td>:&nbsp;</td>
			<td><?php echo $row['agama'] ?></td>
		</tr>
		
		<tr class="spaceUnder">
			<td>No. HP</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row['no_hp'] ?></td>
			<td>E-mail</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row['emel'] ?></td>
		</tr>
		
		<tr class="spaceUnder">
			<td>Pekerjaan</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row['pekerjaan'] ?></td>
			<td>Pendapatan Bulanan</td>
			<td>:&nbsp;</td>
			<td><?php echo $row['pendapatan'] ?></td>
		</tr>
		
        <tr class="spaceUnder">
			<td>Tahap Pendidikan</td>
            <td>:&nbsp;</td>
			<td><?php echo $row['pendidikan'] ?></td>
			<td>Jumlah Bilangan Anak</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row['jumlah_anak'] ?></td>
		</tr>
		
        <tr class="spaceUnder">
			<td>Alamat</td>
			<td>:</td>			
			<td>
				<?php echo $row['alamat_1'];?><br>
				<?php echo $row['alamat_2'];?><br>
				<?php echo $row['poskod'];?> <?php echo $row['bandar'];?><br>
				<?php echo $row['negeri'];?>
			</td>
			<td>Qaryah</td> 
			<td>:&nbsp;</td>
			<?php
			$query_mukim = "SELECT * FROM mukim where mukim_id = {$row['ketua_mukim']}";
			$result_mukim = mysqli_query($connect, $query_mukim);
			while($row_mukim = mysqli_fetch_assoc($result_mukim)){
				$mukim_id = $row_mukim['mukim_id'];
				$mukim_nama = $row_mukim['mukim_nama'];
			}
			?>
			<td><?php echo $mukim_nama ?></td>
		</tr>
		
	</table><br>
	<!--End Table Ketua Keluarga-->
	
	<?php
    if($count2 != 0){
        
        ?>
        
		<!--Table Isteri-->
		<b><i>Maklumat Isteri/Waris:</i></b><br>
		<table align="center" frame="" width="100%">	
    
        <?php
        
        while ($row2=mysqli_fetch_array($result2)){
        ?>
        
        <br>
           
        <tr class="spaceUnder">
			<td width="17%">Nama</td>
            <td width="1%">:&nbsp;</td>
			<td width="31%"><?php echo $row2['nama'] ?></td>
			<td width="17%">No K/P</td> 
			<td width="1%">:&nbsp;</td>
			<td width="31%"><?php echo $row2['no_kp'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tarikh Lahir</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row2['tarikh_lahir'] ?></td>
			<td>Jantina</td>
			<td>:&nbsp;</td>
			<td><?php echo $row2['jantina'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tempat Kelahiran</td>
			<td>:&nbsp;</td>
			<td><?php echo $row2['tempat_lahir'] ?></td>
			<td>No. HP</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row2['no_hp'] ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Pekerjaan</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row2['pekerjaan'] ?></td>
			<td>Pendapatan Bulanan</td><br>
			<td>:&nbsp;</td>
			<td><?php echo $row2['pendapatan'] ?></td>
		</tr>
		
		<tr class="spaceUnder">
			<td>Tahap Pendidikan</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row2['pendidikan'] ?></td>
		</tr>
        <?php
        $i++;
        }
    }
    ?>
	</table>
	<!--End Table Isteri -->
	
    <?php
    if($count3 != 0){
        
        ?>
        
        <!--Table Tangungan 1-->
	    <b><i>Maklumat Anak Dalam Tangungan Yang Belum Berkahwin:</i></b>
<!--	<b><i>Tangungan Pertama</i></b>-->
	    <table align="center" frame="" width="100%">
    
        <?php
        
        while ($row3 = mysqli_fetch_array($result3)){
        ?>
        
        <br>
           
        <tr class="spaceUnder">
			<td width="17%">Nama</td>
            <td width="1%">:&nbsp;</td>
			<td width="31%"><?php echo $row3['nama']; ?></td>
			<td width="17%">No K/P</td> 
			<td width="1%">:&nbsp;</td>
			<td width="31%"><?php echo $row3['no_kp']; ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tarikh Lahir</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row3['tarikh_lahir']; ?></td>
			<td>Jantina</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row3['jantina']; ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Pekerjaan</td>
			<td>:&nbsp;</td>
			<td><?php echo $row3['pekerjaan']; ?></td>
			<td>Pendapatan Bulanan</td> 
			<td>:&nbsp;</td>
			<td><?php echo $row3['pendapatan']; ?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Tahap Pendidikan</td>
			<td>:&nbsp;</td>			
			<td><?php echo $row3['pendidikan']; ?></td>
			<td>Pusat Pendidkan</td>
			<td>:&nbsp;</td>
			<td><?php echo $row3['pusat_pendidikan']; ?></td>
		</tr>
        <?php
        $i++;
        }
    }
    ?>
	</table>
	<!--End Table Tangungan 1-->
	

	<!--Table Lain-lain Maklumat-->
	<br>
	<b><i>Lain-lain Maklumat:</i></b><br>
	<table align="center" frame="" width="100%">		
		
		<tr class="spaceUnder">
			<td width="18%">Keluasan Tanah Yang Diusahakan</td>
            <td width="1%">:&nbsp;</td>
			<td width="81%"><?php echo $row['luas_tanah'];?></td>
		</tr>
		<tr class="spaceUnder">
			<td>Kilang Kedai Yang Diusahakan</td>
            <td>:&nbsp;</td>
			<td><?php echo $row['kilang_kedai'];?></td>
		</tr>
	</table>
	<table align="center" frame="" width="100%">		
		<tr class="spaceUnder">
			<td width="20.6%">Jumlah Kenderaan</td>
            <td width="1%">:&nbsp;</td>
            
            <td width="4%">Kereta</td>
            <td width="1%">:&nbsp;</td>
            <td width="15.6%"><?php echo $row['jumlah_kereta'];?></td>
            
            <td width="6%">Motorsikal</td>
            <td width="1%">:&nbsp;</td>
            <td width="12.6%"><?php echo $row['jumlah_motosikal'];?></td>
            
            <td width="2%">Lori</td>
            <td width="1%">:&nbsp;</td>
            <td width="17.6%"><?php echo $row['jumlah_lori'];?></td>
            
            <td width="2%">Bas</td>
            <td width="1%">:&nbsp;</td>
            <td width="17.6%"><?php echo $row['jumlah_bas'];?></td>
		</tr>
	</table><br>
	<!--End Lain-lain Maklumat-->
<!--Table Kegunaan MPKK-->	
<table style="border-collapse: collapse; width: 100%;" border="1" >
    <tbody>
        <tr>
            <td style="width: 100%; padding-left: 10px"><strong>Untuk Kegunaan MPKK:-</strong></td>
        </tr>
        <tr>
            <td style="width: 100%; padding-left: 10px">Keseluruhan Pendapatan Bulanan Isi Rumah :</td>
        </tr>
        <tr>
            <td style="width: 100%; padding-left: 10px">Purata Jumlah Umur Dalam Keluarga:           
                <table align="center" frame="" width="100%">		
                    <tr>
                        <td width="50%; padding-left: 10px">
                            <p><b>65 Tahun Keatas&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                        <td width="50%">
                            <p><b>41 - 64 Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <p><b>20 - 40 Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                        <td width="50%">
                            <p><b>13 - 19 Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <p><b>07 - 12 Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                        <td width="50%">
                            <p><b>05 - 06 Tahun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> Lelaki...... Wanita......</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <p><b>4 Tahun Kebawah:</b> Lelaki...... Wanita......</p>
                        </td>
                        <td width="50%">
                            <p><b>Jumlah Keseluruhan:</b> Lelaki...... Wanita......</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</div>
<!--Table Kegunaan MPKK-->	
		
</div>
</div>
<div>
      
       
    <br>
    <p align="center">
        <button onclick="printContent('div1')" class="btn btn-success">Cetak</button>
        <a type="link" href="ketua_view.php?id=<?php echo $id?>" class="btn btn-danger">Batal</a>
    </p>
        
</div>

<script>
    function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
    }
</script>