<?php session_start(); ?>

<section>
	<!-- Left Sidebar -->
	<aside id="leftsidebar" class="sidebar">
		<!-- User Info -->
		<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
		<div class="user-info">
			<div class="image">
				<img src="/aksesori_kereta/images/<?php echo $_SESSION["jenis_pengguna"]; ?>.jpg" width="48" height="48" alt="User" />
			</div>
			<div class="info-container">
				<a style="display:block" href="/aksesori_kereta/pages/tambah_aksesori_luaran.php"><div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ucwords(strtolower( $_SESSION["nama_pengguna"])); ?></div></a>
				<div class="email"> <?php echo ucwords(strtolower( $_SESSION["jenis_pengguna"])); ?> </div>

			</div>
		</div>
		<?php } ?>
		<!-- #User Info -->
		<!-- Menu -->
		<div class="menu">
			<ul class="list">
				
					<li>
						<a href="/qaryah/pages/home.php">
							<i class="material-icons">home</i>
							<span>Utama</span>
						</a>
					</li>

					<li>
						<a href="/qaryah/pages/qaryah_insert.php">
							<i class="material-icons">view_list</i>
							<span>Daftar Qaryah</span>
						</a>
					</li>
					
					<li>
						<a href="javascript:void(0)" class="menu-toggle">
						<i class="material-icons">view_list</i>
						<span>Jawatankuasa</span>
						</a>
						<ul class="ml-menu">
							<li>
								<a href="/qaryah/pages/jawatankuasamasjid.php"><span>Jawatankuasa Masjid</span></a>
							</li>
							<li>
								<a href="/qaryah/pages/jawatankuasamasjidjohor.php"><span>Johor</span></a>
							</li>
							<li>
								<a href="/qaryah/pages/jawatankuasamasjidbatupahat.php"><span>Batu Pahat</span></a>
							</li>
							<li>
								<a href="/qaryah/pages/jawatankuasamasjidfathululum.php"><span>Masjid Fathul Ulum Parit Raja</span></a>
							</li>
							<li>
								<a href="javascript:void(0);" class="menu-toggle">
									<span>Maklumat</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="javascript:void(0);">
											<span>Senarai AJK</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0);">
											<span>Cipta AJK</span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
				<?php if(!isset($_SESSION["jenis_pengguna"])){ ?>
					<li>
						<a href="/qaryah/index.html">
							<i class="material-icons">check</i>
							<span>Login</span>
						</a>
					</li>
				<?php }; ?>
				<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
					<li>
						<a href="/qaryah/logout.php">
							<i class="material-icons">input</i>
							<span>Log keluar</span>
						</a>
					</li>
				
				<?php }; ?>

					
				

			</ul>
		</div>
		<!-- #Menu -->
		<!-- Footer -->
		<div class="legal">
			<div class="copyright">
				&copy; 2019 - 2020 <a href="javascript:void(0);">Pusat ICT UTHM</a>.
			</div>
			<div class="version">
				<b>Version: </b> 1.0.5
			</div>
		</div>
		<!-- #Footer -->
	</aside>
	<!-- #END# Left Sidebar -->
	<!-- Right Sidebar -->
	<aside id="rightsidebar" class="right-sidebar">
		<ul class="nav nav-tabs tab-nav-right" role="tablist">
			<li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
			<li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active in active" id="skins">
				<ul class="demo-choose-skin">
					<li data-theme="red" class="active">
						<div class="red"></div>
						<span>Red</span>
					</li>
					<li data-theme="pink">
						<div class="pink"></div>
						<span>Pink</span>
					</li>
					<li data-theme="purple">
						<div class="purple"></div>
						<span>Purple</span>
					</li>
					<li data-theme="deep-purple">
						<div class="deep-purple"></div>
						<span>Deep Purple</span>
					</li>
					<li data-theme="indigo">
						<div class="indigo"></div>
						<span>Indigo</span>
					</li>
					<li data-theme="blue">
						<div class="blue"></div>
						<span>Blue</span>
					</li>
					<li data-theme="light-blue">
						<div class="light-blue"></div>
						<span>Light Blue</span>
					</li>
					<li data-theme="cyan">
						<div class="cyan"></div>
						<span>Cyan</span>
					</li>
					<li data-theme="teal">
						<div class="teal"></div>
						<span>Teal</span>
					</li>
					<li data-theme="green">
						<div class="green"></div>
						<span>Green</span>
					</li>
					<li data-theme="light-green">
						<div class="light-green"></div>
						<span>Light Green</span>
					</li>
					<li data-theme="lime">
						<div class="lime"></div>
						<span>Lime</span>
					</li>
					<li data-theme="yellow">
						<div class="yellow"></div>
						<span>Yellow</span>
					</li>
					<li data-theme="amber">
						<div class="amber"></div>
						<span>Amber</span>
					</li>
					<li data-theme="orange">
						<div class="orange"></div>
						<span>Orange</span>
					</li>
					<li data-theme="deep-orange">
						<div class="deep-orange"></div>
						<span>Deep Orange</span>
					</li>
					<li data-theme="brown">
						<div class="brown"></div>
						<span>Brown</span>
					</li>
					<li data-theme="grey">
						<div class="grey"></div>
						<span>Grey</span>
					</li>
					<li data-theme="blue-grey">
						<div class="blue-grey"></div>
						<span>Blue Grey</span>
					</li>
					<li data-theme="black">
						<div class="black"></div>
						<span>Black</span>
					</li>
				</ul>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="settings">
				<div class="demo-settings">
					<p>GENERAL SETTINGS</p>
					<ul class="setting-list">
						<li>
							<span>Report Panel Usage</span>
							<div class="switch">
								<label><input type="checkbox" checked><span class="lever"></span></label>
							</div>
						</li>
						<li>
							<span>Email Redirect</span>
							<div class="switch">
								<label><input type="checkbox"><span class="lever"></span></label>
							</div>
						</li>
					</ul>
					<p>SYSTEM SETTINGS</p>
					<ul class="setting-list">
						<li>
							<span>Notifications</span>
							<div class="switch">
								<label><input type="checkbox" checked><span class="lever"></span></label>
							</div>
						</li>
						<li>
							<span>Auto Updates</span>
							<div class="switch">
								<label><input type="checkbox" checked><span class="lever"></span></label>
							</div>
						</li>
					</ul>
					<p>ACCOUNT SETTINGS</p>
					<ul class="setting-list">
						<li>
							<span>Offline</span>
							<div class="switch">
								<label><input type="checkbox"><span class="lever"></span></label>
							</div>
						</li>
						<li>
							<span>Location Permission</span>
							<div class="switch">
								<label><input type="checkbox" checked><span class="lever"></span></label>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</aside>
	<!-- #END# Right Sidebar -->
</section>
