	<!-- Jquery Core Js -->
    <script src="/aksesori_kereta/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/aksesori_kereta/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="/aksesori_kereta/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/aksesori_kereta/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/aksesori_kereta/plugins/node-waves/waves.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="/aksesori_kereta/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="/aksesori_kereta/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/aksesori_kereta/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="/aksesori_kereta/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Custom Js -->
    <script src="/aksesori_kereta/js/admin.js"></script>
    <script src="/aksesori_kereta/js/pages/forms/basic-form-elements.js"></script>

    <!-- Demo Js -->
    <script src="/aksesori_kereta/js/demo.js"></script>
 
	  
</html>
