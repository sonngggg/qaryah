<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>


<?php
$table = 'isteri_waris';
$id = $_GET['id'];
$id_ketua = $_GET['id_ketua'];
$nama_ketua = $_GET['nama'];

$saved = 0;

$query = "SELECT * FROM $table WHERE no_kp ='$id'";
    $result = mysqli_query($connect, $query);
    $count = mysqli_num_rows($result);

    $row=mysqli_fetch_array($result);

$_POST['save'] = NULL;

foreach ($_POST as $key=>$value){
    if ($value==''){
        $_POST[$key]='0';
    }
}

$no_kp_ketua = isset($_POST['no_kp_ketua']) ? mysqli_real_escape_string($connect, $_POST['no_kp_ketua']) : '';

$no_kp = isset($_POST['no_kp']) ? mysqli_real_escape_string($connect, $_POST['no_kp']) : '';
$nama = isset($_POST['nama']) ? mysqli_real_escape_string($connect, $_POST['nama']) : '';
$tarikh_lahir = isset($_POST['tarikh_lahir']) ? mysqli_real_escape_string($connect, $_POST['tarikh_lahir']) : '';
$jantina = isset($_POST['jantina']) ? mysqli_real_escape_string($connect, $_POST['jantina']) : '';
$tempat_lahir = isset($_POST['tempat_lahir']) ? mysqli_real_escape_string($connect, $_POST['tempat_lahir']) : '';
$no_hp = isset($_POST['no_hp']) ? mysqli_real_escape_string($connect, $_POST['no_hp']) : '';
$pekerjaan = isset($_POST['pekerjaan']) ? mysqli_real_escape_string($connect, $_POST['pekerjaan']) : '';
$pendapatan = isset($_POST['pendapatan']) ? mysqli_real_escape_string($connect, $_POST['pendapatan']) : '';
$pendidikan = isset($_POST['pendidikan']) ? mysqli_real_escape_string($connect, $_POST['pendidikan']) : '';

$nama = strtoupper($nama);
$tempat_lahir = strtoupper($tempat_lahir);
$pekerjaan = strtoupper($pekerjaan);

if (isset($_POST['save'])) {
    $update_query = "UPDATE $table SET nama='$nama', tarikh_lahir='$tarikh_lahir', jantina='$jantina', tempat_lahir='$tempat_lahir', no_hp='$no_hp', pekerjaan='$pekerjaan', pendapatan='$pendapatan', pendidikan='$pendidikan' WHERE no_kp='$no_kp' ";

    $update_result = mysqli_query($connect, $update_query);
    
}

if ($no_kp != "") {
    echo "<script>location.href='ketua_view.php?id=$no_kp_ketua'</script>";
    exit;
}

?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<form method="post" action="">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<div>
								<b><i>B - Maklumat Isteri/Waris :</i></b>
								</div>
								<br><br>Nama Ketua Keluarga: <?php echo $nama_ketua; ?>
								<input type="hidden" name="no_kp_ketua" value="<?php echo $id_ketua; ?>" placeholder="No Kad Pengenalan" required>
								<br>
									
								<tr class="spaceunder">
									<td>Nama</td>
									<td>:</td>
									<td><input type="text" name="nama" id="" value="<?php echo $row['nama']; ?>" class="form-control" autocomplete="off" required></td>
									<td>&nbsp; No K/P</td> 
									<td>:</td>
									<td><input type="text" name="no_kp" id="" value="<?php echo $row['no_kp']; ?>" class="form-control" autocomplete="off" placeholder="Contoh: 721205016457" required></td>
								</tr>
								<tr class="spaceunder">
									<td>Tarikh Lahir:</td> 
									<td>:</td>
									<td><input type="date" autocomplete="off" name="tarikh_lahir" value="<?php echo $row['tarikh_lahir']; ?>" id="" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Jantina</td> 
									<td>:</td>
									<td>
										<input id="LELAKI" class="with-gap" type="radio" name="jantina" value="LELAKI"<?php echo ($row['jantina']=='LELAKI')?'checked':'' ?> required><label for="LELAKI">LELAKI</label>
										<input id="PEREMPUAN" class="with-gap" type="radio" name="jantina" value="PEREMPUAN"<?php echo ($row['jantina']=='PEREMPUAN')?'checked':'' ?> required><label for="PEREMPUAN">PEREMPUAN</label>
									</td>
								</tr>
								<tr class="spaceunder">
									<td>Tempat Kelahiran</td>
									<td>:</td>
									<td><input type="text" name="tempat_lahir" id="" value="<?php echo $row['tempat_lahir']; ?>" class="form-control" autocomplete="off"></td>
									<td>&nbsp; No. HP</td>
									<td>:</td>			
									<td><input type="text" placeholder="Contoh: 0196547863" name="no_hp" id="" value="<?php echo strtoupper($row['no_hp']); ?>" class="form-control" autocomplete="off"></td>
								</tr>
								
								<tr class="spaceunder">
									<td>Pekerjaan</td>
									<td>:</td>			
									<td><input type="text" name="pekerjaan" id="" value="<?php echo $row['pekerjaan']; ?>" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Pendapatan Bulanan</td> 
									<td>:</td>
									<td><input type="text" placeholder="Contoh: 1200.50" name="pendapatan" id="" value="<?php echo $row['pendapatan']; ?>" class="form-control" autocomplete="off"></td>
								</tr>
								
								<tr class="spaceunder">
									<td>Tahap Pendidikan</td>
									<td>:</td>			
									<td>
										<select class="form-control" id="pendidikan" name="pendidikan" required>
											<option value="UPSR" <?php echo ($row['pendidikan']=='UPSR') ?'selected':'' ?> required>UPSR</option>
											<option value="PT3/SRP/LCE" <?php echo ($row['pendidikan']=='PT3/SRP/LCE') ?'selected':'' ?> required >PT3/SRP/LCE</option>
											<option value="SPM/MCE" <?php echo ($row['pendidikan']=='SPM/MCE') ?'selected':'' ?> required>SPM/MCE</option>
											<option value="STPM" <?php echo ($row['pendidikan']=='STPM') ?'selected':'' ?> required>STPM</option>
											<option value="DIPLOMA" <?php echo ($row['pendidikan']=='DIPLOMA') ?'selected':'' ?> required>DIPLOMA</option>
											<option value="SARJANA MUDA" <?php echo ($row['pendidikan']=='SARJANA MUDA') ?'selected':'' ?> required>SARJANA MUDA</option>
											<option value="SARJANA" <?php echo ($row['pendidikan']=='SARJANA') ?'selected':'' ?> required>SARJANA</option>
											<option value="PhD" <?php echo ($row['pendidikan']=='PhD') ?'selected':'' ?> required>PhD</option>
										</select>
								  </td>
								</tr>	 
								</table
							</div>
							<div align="center">
								<input type="submit" name="save" value="Simpan" class="link btn btn-success">
								<button type="cancel" onclick="ketua_view.php?id=<?php echo $no_kp_ketua?>" class="btn btn-danger">Batal</button>
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<?php include "../pages/template/footer.php"; ?>

