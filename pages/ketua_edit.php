<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>

<?php
$table = 'ketua_keluarga';
$id = $_GET['id'];

foreach ($_POST as $key=>$value){
    if ($value==''){
        $_POST[$key]='0';
    }
}

$query = "SELECT * FROM ketua_keluarga WHERE no_kp ='$id'";
$result = mysqli_query($connect, $query);
$count = mysqli_num_rows($result);

$row=mysqli_fetch_array($result);

$no_kp = isset($_POST['no_kp']) ? mysqli_real_escape_string($connect, $_POST['no_kp']) : '';
$nama = isset($_POST['nama']) ? mysqli_real_escape_string($connect, $_POST['nama']) : '';
$tarikh_lahir = isset($_POST['tarikh_lahir']) ? mysqli_real_escape_string($connect, $_POST['tarikh_lahir']) : '';
$jantina = isset($_POST['jantina']) ? mysqli_real_escape_string($connect, $_POST['jantina']) : '';
$tempat_lahir = isset($_POST['tempat_lahir']) ? mysqli_real_escape_string($connect, $_POST['tempat_lahir']) : '';
$keturunan = isset($_POST['keturunan']) ? mysqli_real_escape_string($connect, $_POST['keturunan']) : '';
$taraf_kahwin = isset($_POST['taraf_kahwin']) ? mysqli_real_escape_string($connect, $_POST['taraf_kahwin']) : '';
$agama = isset($_POST['agama']) ? mysqli_real_escape_string($connect, $_POST['agama']) : '';
$no_hp = isset($_POST['no_hp']) ? mysqli_real_escape_string($connect, $_POST['no_hp']) : '';
$emel = isset($_POST['emel']) ? mysqli_real_escape_string($connect, $_POST['emel']) : '';
$pekerjaan = isset($_POST['pekerjaan']) ? mysqli_real_escape_string($connect, $_POST['pekerjaan']) : '';
$pendapatan = isset($_POST['pendapatan']) ? mysqli_real_escape_string($connect, $_POST['pendapatan']) : '';
$pendidikan = isset($_POST['pendidikan']) ? mysqli_real_escape_string($connect, $_POST['pendidikan']) : '';
$jumlah_anak = isset($_POST['jumlah_anak']) ? mysqli_real_escape_string($connect, $_POST['jumlah_anak']) : '';
$alamat_1= isset($_POST['alamat_1']) ? mysqli_real_escape_string($connect, $_POST['alamat_1']) : '';
$alamat_2 = isset($_POST['alamat_2']) ? mysqli_real_escape_string($connect, $_POST['alamat_2']) : '';
$poskod = isset($_POST['poskod']) ? mysqli_real_escape_string($connect, $_POST['poskod']) : '';
$bandar = isset($_POST['bandar']) ? mysqli_real_escape_string($connect, $_POST['bandar']) : '';
$negeri = isset($_POST['negeri']) ? mysqli_real_escape_string($connect, $_POST['negeri']) : '';
$ketua_mukim = isset($_POST['mukim']) ? mysqli_real_escape_string($connect, $_POST['mukim']) : '';
//    $luas_tanah = isset($_POST['luas_tanah']) ? mysqli_real_escape_string($connect, $_POST['luas_tanah']) : '';
//    $kilang_kedai = isset($_POST['kilang_kedai']) ? mysqli_real_escape_string($connect, $_POST['kilang_kedai']) : '';
//    $jumlah_kereta = isset($_POST['jumlah_kereta']) ? mysqli_real_escape_string($connect, $_POST['jumlah_kereta']) : '';
//    $jumlah_motosikal = isset($_POST['jumlah_motosikal']) ? mysqli_real_escape_string($connect, $_POST['jumlah_motosikal']) : '';
//    $jumlah_lori = isset($_POST['jumlah_lori']) ? mysqli_real_escape_string($connect, $_POST['jumlah_lori']) : '';
//    $jumlah_bas = isset($_POST['jumlah_bas']) ? mysqli_real_escape_string($connect, $_POST['jumlah_bas']) : '';

$nama = strtoupper($nama);
$tempat_lahir = strtoupper($tempat_lahir);
$pekerjaan = strtoupper($pekerjaan);
$alamat_1 = strtoupper($alamat_1);
$alamat_2 = strtoupper($alamat_2);
$bandar = strtoupper($bandar);
$negeri = strtoupper($negeri);

if (isset($_POST['save'])) {
	$update_query = "UPDATE $table  SET nama='$nama', tarikh_lahir='$tarikh_lahir', jantina='$jantina', tempat_lahir='$tempat_lahir',
	keturunan='$keturunan', taraf_kahwin='$taraf_kahwin', agama='$agama', no_hp='$no_hp', emel='$emel', pekerjaan='$pekerjaan', 
	pendapatan=$pendapatan, pendidikan='$pendidikan', jumlah_anak=$jumlah_anak, alamat_1='$alamat_1', alamat_2='$alamat_2', 
	poskod='$poskod', bandar='$bandar', negeri='$negeri', ketua_mukim='$ketua_mukim' WHERE no_kp='$no_kp' "; 
	$update_result = mysqli_query($connect, $update_query);
}

if ($no_kp != "") {
	echo "<script>location.href='ketua_view.php?id=$no_kp'</script>";
	exit;
}
?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<form method="post" action="">
							<input type="hidden" name="no_kp_ketua" value="<?php echo $id_ketua; ?>" placeholder="No Kad Pengenalan" required>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
									<div>
										<b><i>A- Maklumat Peribadi Ketua Keluarga:</i></b>
									</div>
							
									<tr>
										<td>Nama</td>
										<td>:</td> <!--ucwords(strtolower( $row['nama'])); -->
										<td><input type="text" name="nama" value="<?php echo strtoupper( $row['nama']);?>" id="" class="form-control" autocomplete="off" required></td>
										<td>&nbsp; No K/P</td> 
										<td>:</td>
										<td> <?php echo $row['no_kp'];?></td>
										<input type="hidden" required name="no_kp" value="<?php echo $row['no_kp'];?>" id="" class="form-control" autocomplete="off" required>
									</tr>

									<tr>
										<td>Tarikh Lahir:</td> 
										<td>:</td>
										<td><input type="date" autocomplete="off" name="tarikh_lahir" id="" value="<?php echo $row['tarikh_lahir'];?>" class="form-control" autocomplete="off"></td>
										<td>&nbsp; Jantina</td> 
										<td>:</td>
										<td> 
										<input id="LELAKI" class="with-gap" type="radio" name="jantina" value="LELAKI"<?php echo ($row['jantina']=='LELAKI')?'checked':'' ?> required><label for="LELAKI">LELAKI</label>
										<input id="PEREMPUAN" class="with-gap" type="radio" name="jantina" value="PEREMPUAN"<?php echo ($row['jantina']=='PEREMPUAN')?'checked':'' ?> required><label for="PEREMPUAN">PEREMPUAN</label>
									</tr>

									<tr>
										<td>Tempat Kelahiran</td>
										<td>:</td>
										<td><input type="text" name="tempat_lahir" value="<?php echo strtoupper( $row['tempat_lahir']);?>" id="" class="form-control" autocomplete="off"></td>
										<td>&nbsp; Keturunan/ Bangsa</td> 
										<td>:</td>
										<td>
										
										<input type="radio" name="keturunan" value="MELAYU" id="MELAYU" class="with-gap"<?php echo ($row['keturunan']=='MELAYU')?'checked':'' ?> required><label for="MELAYU">MELAYU</label>
										<input type="radio" name="keturunan" value="CINA" id="CINA" class="with-gap"<?php echo ($row['keturunan']=='CINA')?'checked':'' ?> required><label for="CINA">CINA</label>
										<input type="radio" name="keturunan" value="INDIA" id="INDIA" class="with-gap"<?php echo ($row['keturunan']=='INDIA')?'checked':'' ?> required><label for="INDIA">INDIA</label>
										<input type="radio" name="keturunan" value="LAIN-LAIN" id="LAIN-LAIN" class="with-gap"<?php echo ($row['keturunan']=='LAIN-LAIN')?'checked':'' ?> required><label for="LAIN-LAIN">LAIN-LAIN</label>
										
										</td>
									</tr>

									<tr>
										<td>Taraf Perkahwinan</td>
										<td>:</td>			
										<td>
										<select class="form-control" id="taraf_kahwin" name="taraf_kahwin" required>
											<option value="BUJANG" <?php echo ($row['taraf_kahwin']=='BUJANG') ?'selected':'' ?> required>BUJANG</option>
											<option value="BERKAHWIN" <?php echo ($row['taraf_kahwin']=='BERKAHWIN') ?'selected':'' ?> required>BERKAHWIN</option>
											<option value="DUDA" <?php echo ($row['taraf_kahwin']=='DUDA') ?'selected':'' ?> required>DUDA</option>
											<option value="JANDA" <?php echo ($row['taraf_kahwin']=='JANDA') ?'selected':'' ?> required>JANDA</option>
										</select>
										</td>
										<td>&nbsp; Agama</td><br>
										<td>:</td>
										<td>
										<select class="form-control" id="agama" name="agama" required>
											<option value="ISLAM" <?php echo ($row['agama']=='ISLAM') ?'selected':'' ?> required>ISLAM</option>
											<option value="BUDHA" <?php echo ($row['agama']=='BUDHA') ?'selected':'' ?> required >BUDHA</option>
											<option value="HINDU" <?php echo ($row['agama']=='HINDU') ?'selected':'' ?> required>HINDU</option>
											<option value="KRISTIAN" <?php echo ($row['agama']=='KRISTIAN') ?'selected':'' ?> required>KRISTIAN</option>
											<option value="SIKH" <?php echo ($row['agama']=='SIKH') ?'selected':'' ?> required>SIKH</option>
											<option value="LAIN-LAIN" <?php echo ($row['agama']=='LAIN-LAIN') ?'selected':'' ?> required>LAIN-LAIN</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>No. HP</td>
										<td>:</td>			
										<td><input type="text" name="no_hp" value="<?php echo $row['no_hp'];?>" id="" class="form-control" autocomplete="off" placeholder="Contoh: 0196547863"></td>
										<td>&nbsp; E-mail</td> 
										<td>:</td>
										<td><input type="email" name="emel" id="" value="<?php echo $row['emel'];?>" class="form-control" autocomplete="off" placeholder="Biarkan kosong jika tiada emel"></td>
									</tr>
									<tr>
										<td>Pekerjaan</td>
										<td>:</td>			
										<td><input type="text" name="pekerjaan" id="" value="<?php echo strtoupper( $row['pekerjaan']);?>" class="form-control" autocomplete="off"></td>
										<td>&nbsp; Pendapatan Bulanan (RM)</td> 
										<td>:</td>
										<td><input type="text" name="pendapatan" id="" value="<?php echo $row['pendapatan'];?>" class="form-control" autocomplete="off" placeholder="Contoh: 1200.50"></td>
									</tr>
									<tr>
										<td>Tahap Pendidikan</td>
										<td>:</td>			
										<td>
											<select class="form-control" id="pendidikan" name="pendidikan" required>
												<option value="UPSR" <?php echo ($row['pendidikan']=='UPSR') ?'selected':'' ?> required>UPSR</option>
												<option value="PT3/SRP/LCE" <?php echo ($row['pendidikan']=='PT3/SRP/LCE') ?'selected':'' ?> required >PT3/SRP/LCE</option>
												<option value="SPM/MCE" <?php echo ($row['pendidikan']=='SPM/MCE') ?'selected':'' ?> required>SPM/MCE</option>
												<option value="STPM" <?php echo ($row['pendidikan']=='STPM') ?'selected':'' ?> required>STPM</option>
												<option value="DIPLOMA" <?php echo ($row['pendidikan']=='DIPLOMA') ?'selected':'' ?> required>DIPLOMA</option>
												<option value="SARJANA MUDA" <?php echo ($row['pendidikan']=='SARJANA MUDA') ?'selected':'' ?> required>SARJANA MUDA</option>
												<option value="SARJANA" <?php echo ($row['pendidikan']=='SARJANA') ?'selected':'' ?> required>SARJANA</option>
												<option value="PhD" <?php echo ($row['pendidikan']=='PhD') ?'selected':'' ?> required>PhD</option>
											</select>
										</td>
										<td>Jumlah Bilangan Anak</td> 
										<td>:</td>
										<td><input type="text" name="jumlah_anak" id="" value="<?php echo $row['jumlah_anak'];?>" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>:</td>			
										<td colspan="4"><input type="text" name="alamat_1" id="" value="<?php echo strtoupper( $row['alamat_1']);?>" class="form-control" autocomplete="off"></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>:</td>			
										<td colspan="4"><input type="text" name="alamat_2" id="" value="<?php echo strtoupper( $row['alamat_2']);?>" class="form-control" autocomplete="off"></td>
									</tr>	
								   
									<tr>
										<td>Poskod</td>
										<td>:</td>			
										<td colspan="1"><input type="text" name="poskod" value="<?php echo $row['poskod'];?>" id="" class="form-control" autocomplete="off" placeholder="Contoh: 86400"></td>
										<td>&nbsp; Bandar</td> 
										<td>:</td>
										<td><input type="text" name="bandar" id="" value="<?php echo strtoupper( $row['bandar']);?>" class="form-control" autocomplete="off"></td>
									</tr>
									
								  <tr>
										<td>Negeri</td> 
										<td>:</td>
										<td><input type="text" name="negeri" id="" value="<?php echo strtoupper( $row['negeri']);?>" class="form-control" autocomplete="off"></td>
										<td>&nbsp; Qaryah</td> 
										<td>:</td>
										<td>
										<select name="mukim" id="" class="form-control">
										<?php 
										$query_mukim_ketua = "SELECT * FROM mukim WHERE mukim_id = {$row['ketua_mukim']}";
										$result_mukim_ketua = mysqli_query($connect, $query_mukim_ketua);
										$row_mukim_ketua = mysqli_fetch_assoc($result_mukim_ketua);

										$query_mukim = "SELECT DISTINCT * FROM mukim WHERE mukim_id != {$row['ketua_mukim']}";
										$result_mukim = mysqli_query($connect, $query_mukim);
										?>

										<option value="<?php echo $row_mukim_ketua['mukim_id'];; ?>"><?php echo $row_mukim_ketua['mukim_nama']; ?></option>
										<?php
										while($row_mukim = mysqli_fetch_array($result_mukim)){
											if($row_mukim['mukim_id'] !== $row_mukim_ketua['ketua_mukim']){
												?>
												<option value="<?php echo  $row_mukim['mukim_id']; ?>"><?php echo $row_mukim['mukim_nama']; ?></option>
												<?php
											}
										}
										?>						
										</select>
										</td>
									</tr>	 
								</table
							</div>
							<div align="center">
									<input type="submit" name="save" value="Simpan" class="link btn btn-success">
								<button class="btn btn-danger" type="cancel" onclick="ketua_view.php?id=<?php echo $no_kp?>">Batal</button>
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<?php include "../pages/template/footer.php"; ?>

