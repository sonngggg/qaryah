<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>


<?php
$table = 'ketua_keluarga';

foreach ($_POST as $key=>$value){
    if ($value==''){
        $_POST[$key]='0';
    }
}

$no_kp = isset($_POST['no_kp']) ? mysqli_real_escape_string($connect, $_POST['no_kp']) : '';
$nama = isset($_POST['nama']) ? mysqli_real_escape_string($connect, $_POST['nama']) : '';
$tarikh_lahir = isset($_POST['tarikh_lahir']) ? mysqli_real_escape_string($connect, $_POST['tarikh_lahir']) : '';
$jantina = isset($_POST['jantina']) ? mysqli_real_escape_string($connect, $_POST['jantina']) : '';
$tempat_lahir = isset($_POST['tempat_lahir']) ? mysqli_real_escape_string($connect, $_POST['tempat_lahir']) : '';
$keturunan = isset($_POST['keturunan']) ? mysqli_real_escape_string($connect, $_POST['keturunan']) : '';
$taraf_kahwin = isset($_POST['taraf_kahwin']) ? mysqli_real_escape_string($connect, $_POST['taraf_kahwin']) : '';
$agama = isset($_POST['agama']) ? mysqli_real_escape_string($connect, $_POST['agama']) : '';
$no_hp = isset($_POST['no_hp']) ? mysqli_real_escape_string($connect, $_POST['no_hp']) : '';
$emel = isset($_POST['emel']) ? mysqli_real_escape_string($connect, $_POST['emel']) : '';
$pekerjaan = isset($_POST['pekerjaan']) ? mysqli_real_escape_string($connect, $_POST['pekerjaan']) : '';
$pendapatan = isset($_POST['pendapatan']) ? mysqli_real_escape_string($connect, $_POST['pendapatan']) : '';
$pendidikan = isset($_POST['pendidikan']) ? mysqli_real_escape_string($connect, $_POST['pendidikan']) : '';
$jumlah_anak = isset($_POST['jumlah_anak']) ? mysqli_real_escape_string($connect, $_POST['jumlah_anak']) : '';
$alamat_1= isset($_POST['alamat_1']) ? mysqli_real_escape_string($connect, $_POST['alamat_1']) : '';
$alamat_2 = isset($_POST['alamat_2']) ? mysqli_real_escape_string($connect, $_POST['alamat_2']) : '';
$poskod = isset($_POST['poskod']) ? mysqli_real_escape_string($connect, $_POST['poskod']) : '';
$bandar = isset($_POST['bandar']) ? mysqli_real_escape_string($connect, $_POST['bandar']) : '';
$negeri = isset($_POST['negeri']) ? mysqli_real_escape_string($connect, $_POST['negeri']) : '';
$luas_tanah = isset($_POST['luas_tanah']) ? mysqli_real_escape_string($connect, $_POST['luas_tanah']) : '';
$kilang_kedai = isset($_POST['kilang_kedai']) ? mysqli_real_escape_string($connect, $_POST['kilang_kedai']) : '';
$jumlah_kereta = isset($_POST['jumlah_kereta']) ? mysqli_real_escape_string($connect, $_POST['jumlah_kereta']) : '';
$jumlah_motosikal = isset($_POST['jumlah_motosikal']) ? mysqli_real_escape_string($connect, $_POST['jumlah_motosikal']) : '';
$jumlah_lori = isset($_POST['jumlah_lori']) ? mysqli_real_escape_string($connect, $_POST['jumlah_lori']) : '';
$jumlah_bas = isset($_POST['jumlah_bas']) ? mysqli_real_escape_string($connect, $_POST['jumlah_bas']) : '';
$ketua_mukim = isset($_POST['mukim']) ? mysqli_real_escape_string($connect, $_POST['mukim']) : '';

$nama = strtoupper($nama);
$tempat_lahir = strtoupper($tempat_lahir);
$pekerjaan = strtoupper($pekerjaan);
$alamat_1 = strtoupper($alamat_1);
$alamat_2 = strtoupper($alamat_2);
$bandar = strtoupper($bandar);
$negeri = strtoupper($negeri);
$luas_tanah = strtoupper($luas_tanah);
$kilang_kedai = strtoupper($kilang_kedai);

if (isset($_POST['save'])) {
    $query = "INSERT INTO $table (no_kp, nama, tarikh_lahir, jantina, tempat_lahir, keturunan, taraf_kahwin, agama, no_hp, emel, pekerjaan, pendapatan, pendidikan, jumlah_anak, alamat_1, alamat_2, poskod, bandar, negeri, luas_tanah, kilang_kedai,jumlah_kereta,jumlah_motosikal,jumlah_lori,jumlah_bas,ketua_mukim) VALUES ('$no_kp', '$nama', '$tarikh_lahir', '$jantina', '$tempat_lahir', '$keturunan', '$taraf_kahwin', '$agama', '$no_hp', '$emel', '$pekerjaan', $pendapatan, '$pendidikan', $jumlah_anak, '$alamat_1', '$alamat_2', '$poskod', '$bandar', '$negeri', '$luas_tanah', '$kilang_kedai', $jumlah_kereta, $jumlah_motosikal, $jumlah_lori, $jumlah_bas, $ketua_mukim)";
    $result = mysqli_query($connect, $query);
    
    
}

if ($no_kp != "") {
    echo "<script>location.href='ketua_view.php?id=$no_kp'</script>";
    exit;
}
?>
<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<form method="post" action="">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<div>
								<b><i>A- Maklumat Peribadi Ketua Keluarga:</i></b>
								</div>
										
								<tr class="spaceunder">
								<td>Nama</td>
								<td>:</td>
								<td><input type="text" name="nama" id="" class="form-control" autocomplete="off" required></td>
								<td>&nbsp; No K/P</td> 
								<td>:</td>
								<td><input type="text" required name="no_kp" id="" class="form-control" autocomplete="off" required placeholder="Contoh: 721205016457"></td>
								</tr>
							  
							  <tr class="spaceunder">
									<td>Tarikh Lahir:</td> 
									<td>:</td>
									<td><input type="date" autocomplete="off" name="tarikh_lahir" id="" class="form-control"></td>
									<td>&nbsp; Jantina</td> 
									<td>:</td>
									<td> 
										<input id="LELAKI" class="with-gap" type="radio" name="jantina" value="LELAKI" required><label for="LELAKI">LELAKI</label>
										<input id="PEREMPUAN" class="with-gap" type="radio" name="jantina" value="PEREMPUAN" required><label for="PEREMPUAN">PEREMPUAN</label>
									</td>
								</tr>
							  
							   <tr class="spaceunder">
									<td>Tempat Kelahiran</td>
									<td>:</td>
									<td><input type="text" name="tempat_lahir" id="" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Keturunan/ Bangsa</td> 
									<td>:</td>
									<td>
									<input type="radio" name="keturunan" value="MELAYU" id="MELAYU" class="with-gap" required><label for="MELAYU">MELAYU</label>
									<input type="radio" name="keturunan" value="CINA" id="CINA" class="with-gap" required><label for="CINA">CINA</label>
									<input type="radio" name="keturunan" value="INDIA" id="INDIA" class="with-gap" required><label for="INDIA">INDIA</label>
									<input type="radio" name="keturunan" value="LAIN-LAIN" id="LAIN-LAIN" class="with-gap" required><label for="LAIN-LAIN">LAIN-LAIN</label>
									</td>
								</tr>
								<tr class="spaceunder">
									<td>Taraf Perkahwinan</td>
									<td>:</td>			
									<td>
									  <select class="form-control" id="pilihan" name="taraf_kahwin" required>
									  <option value="">-- Pilih --</option>
									  <option value="BUJANG">BUJANG</option>
									  <option value="BERKAHWIN">BERKAHWIN</option>
									  <option value="DUDA">DUDA</option>
									  <option value="JANDA">JANDA</option>
										</select>
									  </td>
									<td>&nbsp; Agama</td><br>
									<td>:</td>
									<td>
									<select class="form-control" id="pilihan" name="agama" required>
									  <option value="">-- Pilih --</option>
									  <option value="ISLAM">ISLAM</option>
									  <option value="BUDHA">BUDHA</option>
									  <option value="HINDU">HINDU</option>
									  <option value="KRISTIAN">KRISTIAN</option>
									  <option value="SIKH">SIKH</option>
									  <option value="LAIN-LAIN">LAIN-LAIN</option>
									</select>
									</td>
								</tr>
							  
								<tr class="spaceunder">
									<td>No. HP</td>
									<td>:</td>			
									<td><input type="text" name="no_hp" id="" class="form-control" autocomplete="off" placeholder="Contoh: 0196547863"></td>
									<td>&nbsp; E-mail</td> 
									<td>:</td>
									<td><input type="email" name="emel" id="" class="form-control" autocomplete="off" placeholder="Biarkan kosong jika tiada emel"></td>
								</tr>
								
								<tr class="spaceunder">
									<td>Pekerjaan</td>
									<td>:</td>			
									<td><input type="text" name="pekerjaan" id="" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Pendapatan Bulanan</td> 
									<td>:</td>
									<td><input type="text" name="pendapatan" id="" class="form-control" autocomplete="off" placeholder="Contoh: 1200.50"></td>
								</tr>
							  
								<tr class="spaceunder">
									<td>Tahap Pendidikan</td>
									<td>:</td>			
									<td>
									  <select class="form-control" id="pilihan" name="pendidikan" required>
										  <option value="">-- Pilih --</option>
										  <option value="SRP/LCE">SRP/LCE</option>
										  <option value="SPM/MCE">SPM/MCE</option>
										  <option value="STPM">STPM</option>
										  <option value="DIPLOMA">DIPLOMA</option>
										  <option value="SARJANA MUDA">SARJANA MUDA</option>
										  <option value="SARJANA">SARJANA</option>
										  <option value="PhD">PhD</option>
									  </select>
									</td>

									<td>Jumlah Bilangan Anak</td> 
									<td>:</td>
									<td><input type="text" name="jumlah_anak" id="" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
								</tr>
							  
								<tr class="spaceunder">
									<td>Alamat</td>
									<td>:</td>			
									<td colspan="4"><input type="text" name="alamat_1" id="" class="form-control" autocomplete="off"></td>
								</tr>
							   
								<tr class="spaceunder">
									<td>Alamat</td>
									<td>:</td>			
									<td colspan="4"><input type="text" name="alamat_2" id="" class="form-control" autocomplete="off"></td>
								</tr>	
							   
								<tr class="spaceunder">
									<td>Poskod</td>
									<td>:</td>			
									<td colspan="1"><input type="text" name="poskod" id="" class="form-control" autocomplete="off" placeholder="Contoh: 86400"></td>
									<td>&nbsp; Bandar</td> 
									<td>:</td>
									<td><input type="text" name="bandar" id="" class="form-control" autocomplete="off"></td>
								</tr>
								
							   <tr class="spaceunder">
									<td>Negeri</td> 
									<td>:</td>
									<td><input type="text" name="negeri" id="" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Qaryah</td> 
									<td>:</td>
									<td>
									<select name="mukim" id="" class="form-control">
									<option value="">-- Pilih --</option>
									<?php 
									$query_mukim = "SELECT * FROM mukim";
									$result_mukim = mysqli_query($connect, $query_mukim);
									while($row_mukim = mysqli_fetch_array($result_mukim)){
										?>

										<option value="<?php echo $row_mukim['mukim_id']; ?>"><?php echo $row_mukim['mukim_nama']; ?></option>

										<?php
									}
									?>						
									</select>
									</td>
								</tr>	 
								</table
									
								 <div>
									<br><b><i> D - Lain-Lain Maklumat</i></b>
									</div>
								<table align="center" frame="" width="100%">
										
								
								<tr class="spaceunder">
									<td width="20%" >Keluasan Tanah Yang Diusahakan</td>
									<td widht="1% " >:</td>
									<td width="79%" ><input type="text" name="luas_tanah" id="" class="form-control" autocomplete="off" placeholder="Contoh: 10 ekar"></td>
								</tr>
								<tr class="spaceunder">
									<td>Kilang/Kedai Yang Diusahakan</td> 
									<td>:</td>
									<td><input type="text" name="kilang_kedai" id="" class="form-control" autocomplete="off" placeholder="Contoh: Restoran Maju"></td>
								</tr>
								
								<tr class="spaceunder">
									<td>Jumlah Kenderaan:</td><br>		
								</tr>
								</table>
								   
								<table align="center" frame="" width="100%">
								<tr class="spaceunder">
									<td>Kereta</td>
									<td>:</td>			
									<td><input type="text" name="jumlah_kereta" id="" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
									<td> Motosikal</td> 
									<td>:</td>
									<td><input type="text" name="jumlah_motosikal" id="" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
									<td> Lori</td> 
									<td>:</td>
									<td><input type="text" name="jumlah_lori" id="" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
									<td> Bas</td> 
									<td>:</td>
									<td><input type="text" name="jumlah_bas" id="" class="form-control" autocomplete="off" placeholder="Contoh: 2"></td>
								</tr>	 
								</table
								</div>
								<div align="center">
								<br><input type="submit" name="save" value="Hantar" class="link btn btn-success">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<?php include "../pages/template/footer.php"; ?>

