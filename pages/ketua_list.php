<?php
include 'header.php';
$table = 'ketua_keluarga';
$query = "SELECT * FROM $table";
$result = mysqli_query($connect, $query);
$count = mysqli_num_rows($result);
$i = 1;
?>
<section>
    <h2>Senarai Keluarga</h2>
    <div class="container">
  
    <form method="post" style="text-align:center" action="ketua_insert.php">
		<input type="submit" name="save" value="Daftar Ketua Keluarga Baharu" class="link btn btn-success">
	</form>
    
    <table class="paleBlueRows">
        <thead>
            <tr>
                <th>No. KP</th>
                <th>Nama</th>
                <th>Keturunan/<br>Bangsa</th>
                <th>Taraf<br>Perkahwinan</th>
                <th>Agama</th>
                <th>No. HP</th>
                <th>Pekerjaan</th>
                <th>Jumlah<br>Anak</th>
                <th>Alamat</th>
                <th>Action</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($count != 0) {
                while ($row = mysqli_fetch_array($result)):
                    ?>
                    <tr>
                        <td width="10%" style="text-align:center"><?php echo $row['no_kp']; ?></td>
                        <td width="15%"><?php echo $row['nama']; ?></td>
                        <td width="10%" style="text-align:center"><?php echo $row['keturunan']; ?></td>
                        <td width="10%" style="text-align:center"><?php echo $row['taraf_kahwin']; ?></td>
                        <td width="8%" style="text-align:center"><?php echo $row['agama']; ?></td>
                        <td width="10%" style="text-align:center"><?php echo $row['no_hp']; ?></td>
                        <td width="10%" style="text-align:center"><?php echo $row['pekerjaan']; ?></td>
                        <td width="7%" style="text-align:center"><?php echo $row['jumlah_anak']; ?></td>
                        <td width="15%"><?php echo $row['alamat_1']; ?><br> 
                            <?php echo $row['alamat_2']; ?><br>
                            <?php echo $row['poskod']; ?>
                            <?php echo $row['bandar']; ?><br>
                            <?php echo $row['negeri']; ?>
                        </td>
                        <td width="5%" style="text-align:center">
                            <a href="ketua_view.php?id=<?php echo $row['no_kp']; ?> " class="btn btn-primary">Lihat</a>  
                        </td>
                           <?php
                            /*echo "<td><a href=\"ketua_list.php?delete={$row['no_kp']}\">Padam</a></td>";*/
                            ?>
                        <td>
                        <a onclick="return confirm('Adakah anda pasti ingin memadam data ini?')" href="ketua_list.php?delete=<?php echo $row['no_kp'] ?>" class="btn btn-danger"><span ></span> Padam </a>
                        </td>
                 
                    </tr>
                    <?php
                    $i++;
                endwhile;
            } else {
                ?>
                <tr>
                    <td colspan="5" style="text-align: center;">Data not found</td>
                </tr>
                <?php
            }
            ?>
            <?php
            if(isset($_GET['delete'])){
                                        
                $padam_kp = $_GET['delete'];
                $query = "DELETE FROM $table WHERE no_kp = $padam_kp";
                $result = mysqli_query($connect, $query);
                
                $query2 = "DELETE FROM isteri_waris WHERE no_kp_ketua = $padam_kp";
                $result2 = mysqli_query($connect, $query2);
                
                $query3 = "DELETE FROM tanggungan WHERE no_kp_ketua = $padam_kp";
                $result3 = mysqli_query($connect, $query3);
                
                echo "<script>location.href='ketua_list.php'</script>";
                if(!$result){
                die("DELETE failed" . mysqli_error($connect));
                }
            }
            //header('Location: ketua_list.php');
            ?>
        </tbody>
    </table>
    </div>
</section>
<?php include 'footer.php'; ?>

