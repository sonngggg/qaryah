<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>


<?php

// $_POST['save'] = NULL;
// foreach ($_POST as $key=>$value){
//     if ($value==''){
//         $_POST[$key]='0';
//     }
// }

if (isset($_POST['save'])) {

		$nama_mukim = $_POST['nama_mukim'];
		$nama_mukim = strtoupper($nama_mukim);
    $query = "INSERT INTO mukim(mukim_nama) VALUES ('$nama_mukim')";
		$result = mysqli_query($connect, $query);
		echo "<script>location.href='qaryah_insert.php'</script>";

}

?>
<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
					<div class="header">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<form method="post" action="">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
									<div>
									<b><i>Daftar Qaryah :</i></b>
									</div>
									<br>
									
									<tr class="spaceunder">
									<td>Nama</td>
									<td>:</td>
									<td><input type="text" name="nama_mukim" id="" class="form-control" autocomplete="off" required></td>
									</tr>
									</table>
							</div>
							<div align="center">
								<input type="submit" name="save" value="Hantar" class="link btn btn-success">
							</div>
						</form>
					</div>
				</div>
			</div>
    </div>


		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="body">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
									<div><b><i>Senarai Qaryah</i></b></div><br><br>
									<thead>
										<tr>
											<th>Mukim ID</th>
											<th>Nama Mukim</th>
											<th>Padam Mukim</th>
										</tr>
									</thead>

									<tbody>
									<?php
									$query_mukim = "SELECT * from mukim";
									$result_mukim = mysqli_query($connect, $query_mukim);
									while($row_mukim = mysqli_fetch_array($result_mukim)){
									
									$mukim_id = $row_mukim['mukim_id'];
									echo "<tr>";
									echo "<td>{$row_mukim['mukim_id']}</td>";
									echo "<td>{$row_mukim['mukim_nama']}</td>";

									// $query_jumlah_k_keluarga = "SELECT * FROM ";
									// $result_jumlah_k_keluarga = mysqli_query($connect, $query_jumlah_k_keluarga);
									// $bilangan_k_keluarga = mysqli_num_rows($result_jumlah_k_keluarga);
									// echo "<td>$bilangan_k_keluarga</td>";
									echo "<td><a href=\"qaryah_insert.php?delete={$row_mukim['mukim_id']}\" class=\"btn btn-danger\">Padam</a></td>";                           
									echo "</tr>";
									}
									
									if(isset($_GET['delete'])){
									$query_mukim_delete = "DELETE FROM mukim WHERE mukim_id = $mukim_id";
									$result_mukim_delete = mysqli_query($connect, $query_mukim_delete);
									echo "<script>location.href='qaryah_insert.php'</script>";

									}

									?>
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		</div>

  </div>
</section>

<?php include "../pages/template/footer.php"; ?>

