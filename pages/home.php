<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>

<?php $table = 'ketua_keluarga';
	  $query = "SELECT * FROM $table";
	  $result = mysqli_query($connect, $query);
	  $count = mysqli_num_rows($result);
	  $i = 1;											?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
		
			<div class="col-xs-20 col-sm-20 col-md-18 col-lg-18">
				<div class="card">
					<div class="body">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
									<li data-target="#carousel-example-generic" data-slide-to="3"></li>
									<li data-target="#carousel-example-generic" data-slide-to="4"></li>
									<li data-target="#carousel-example-generic" data-slide-to="5"></li>
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox" align="center">
									<div class="item active">
											<img src="../images/masjid/2.png" />
									</div>
									<div class="item">
											<img src="../images/masjid/a.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/b.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/c.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/d.jpg" />
									</div>
									<div class="item">
										<img src="../images/masjid/e.jpg" />
									</div>
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- #END# Basic Example -->
		
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<h2>
							Senarai Keluarga
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<?php if(isset($_SESSION["nama_pengguna"])){ ?>
									<form method="post" style="text-align:center" action="ketua_insert.php">
										<input type="submit" name="save" value="Daftar Ketua Keluarga Baharu" class="link btn btn-success">
									</form>
								<?php } ?>
								
								<!--<table class="paleBlueRows">-->
									<thead>
										<tr>
											<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
												<th>No. KP</th>
											<?php } ?>
												<th>Nama</th>
												<th>Keturunan/<br>Bangsa</th>
												<th>Taraf<br>Perkahwinan</th>
												<th>Agama</th>
											<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
												<th>No. HP</th>
											<?php } ?>
												<th>Pekerjaan</th>
												<th>Jumlah<br>Anak</th>
												<th>Alamat</th>
											<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
												<th></th>
												<th></th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($count != 0) {
											while ($row = mysqli_fetch_array($result)):
												?>
												<tr>
													<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
														<td width="10%" style="text-align:center"><?php echo $row['no_kp']; ?></td>
													<?php } ?>
														<td width="15%"><?php echo $row['nama']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row['keturunan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row['taraf_kahwin']; ?></td>
														<td width="8%" style="text-align:center"><?php echo $row['agama']; ?></td>
													<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
														<td width="10%" style="text-align:center"><?php echo $row['no_hp']; ?></td>
													<?php } ?>
														<td width="10%" style="text-align:center"><?php echo $row['pekerjaan']; ?></td>
														<td width="7%" style="text-align:center"><?php echo $row['jumlah_anak']; ?></td>
														<td width="15%"><?php echo $row['alamat_1']; ?><br> 
															<?php echo $row['alamat_2']; ?><br>
															<?php echo $row['poskod']; ?>
															<?php echo $row['bandar']; ?><br>
															<?php echo $row['negeri']; ?>
														</td>
														<?php if(isset($_SESSION["jenis_pengguna"])){ ?>
														<td width="5%" style="text-align:center">
															<a href="ketua_view.php?id=<?php echo $row['no_kp']; ?> " class="btn btn-primary">Lihat</a>  
														</td>
														   <?php
															/*echo "<td><a href=\"ketua_list.php?delete={$row['no_kp']}\">Padam</a></td>";*/
															?>
														<td>
														<a onclick="return confirm('Adakah anda pasti ingin memadam data ini?')" href="home.php?delete=<?php echo $row['no_kp'] ?>" class="btn btn-danger"><span ></span> Padam </a>
														</td>
														<?php } ?>
											 
												</tr>
												<?php
												$i++;
											endwhile;
										} else {
											?>
											<tr>
												<td colspan="5" style="text-align: center;">Data not found</td>
											</tr>
											<?php
										}
										?>
										<?php
										if(isset($_GET['delete'])){
																	
											$padam_kp = $_GET['delete'];
											$query = "DELETE FROM $table WHERE no_kp = $padam_kp";
											$result = mysqli_query($connect, $query);
											
											$query2 = "DELETE FROM isteri_waris WHERE no_kp_ketua = $padam_kp";
											$result2 = mysqli_query($connect, $query2);
											
											$query3 = "DELETE FROM tanggungan WHERE no_kp_ketua = $padam_kp";
											$result3 = mysqli_query($connect, $query3);
											
											echo "<script>location.href='home.php'</script>";
											if(!$result){
											die("DELETE failed" . mysqli_error($connect));
											}
										}
										?>
									</tbody>
								<!--</table>-->
							</table>
						</div>
					</div>
				</div>
			</div>
		<div>
	</div>
</section>
<?php include "../pages/template/footer.php"; ?>
</body>

</html>
