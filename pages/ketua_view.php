<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>

<?php 	$table = 'ketua_keluarga';
		$id = $_GET['id'];
		$no_kp_from = $id;
		$i = 1;

		$query = "SELECT * FROM ketua_keluarga WHERE no_kp = '$id'";
		$result = mysqli_query($connect, $query);
		$count = mysqli_num_rows($result);
		$row=mysqli_fetch_array($result);

		$query2 = "SELECT * FROM isteri_waris WHERE no_kp_ketua = '$id'";
		$result2 = mysqli_query($connect, $query2);
		$count2 = mysqli_num_rows($result2);
		$row2=mysqli_fetch_array($result2);

		$query3 = "SELECT * FROM tanggungan WHERE no_kp_ketua ='$id'";
		$result3 = mysqli_query($connect, $query3);
		$count3 = mysqli_num_rows($result3);
		//$row3 = mysqli_fetch_array($result3);			?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Senarai ketua, waris/isteri, tanggungan, aset</h2>
		</div>
		
		<!--KETUA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      		<div class="card">
					<div class="header">
						<h2>
							A- Maklumat Peribadi Ketua Keluarga:
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="35%">No KP</td>
									<td width="65%"><?php echo $row['no_kp'] ?></td>
								</tr>
								
								<tr>
									<td>Nama</td>
									<td><?php echo $row['nama'] ?></td>
								</tr>
								
								<tr >
									<td>Tarikh Lahir</td>
									<td><?php echo $row['tarikh_lahir'] ?></td>
								</tr>
								
								<tr >
									<td>Jantina</td>
									<td><?php echo $row['jantina'] ?></td>
								</tr>
								
								<tr >
									<td>Tempat Lahir</td>
									<td><?php echo $row['tempat_lahir'] ?></td>
								</tr>
								
								<tr >
									<td>Keturunan/Bangsa</td>
									<td><?php echo $row['keturunan'] ?></td>
								</tr>
								
								<tr >
									<td>Taraf Perkahwinan</td>
									<td><?php echo $row['taraf_kahwin'] ?></td>
								</tr>
								
								<tr >
									<td>Agama</td>
									<td><?php echo $row['agama'] ?></td>
								</tr>
								
								<tr >
									<td>No HP</td>
									<td><?php echo $row['no_hp'] ?></td>
								</tr>
						
								<tr >
									<td>Emel</td>
									<td><?php echo $row['emel'] ?></td>
								</tr>
								
								<tr >
									<td>Pekerjaan</td>
									<td><?php echo $row['pekerjaan'] ?></td>
								</tr>
								
								<tr >
									<td>Pendapatan</td>
									<td>RM<?php echo $row['pendapatan'] ?></td>
								</tr>
								
								<tr >
									<td>Tahap Pendidikan</td>
									<td><?php echo $row['pendidikan'] ?></td>
								</tr>
								
								<tr >
									<td>Jumlah Anak</td>
									<td><?php echo $row['jumlah_anak'] ?></td>
								</tr>
								
								<tr>
									<td>Alamat</td>
									<td><?php echo $row['alamat_1'];?><br>
										<?php echo $row['alamat_2'];?><br>
										<?php echo $row['poskod'];?> <?php echo $row['bandar'];?><br>
										<?php echo $row['negeri'];?>
									</td>
								</tr>

								<tr >
									<td>Qaryah</td>
									<?php
									$query_mukim = "SELECT * FROM mukim where mukim_id = {$row['ketua_mukim']}";
									$result_mukim = mysqli_query($connect, $query_mukim);
									while($row_mukim = mysqli_fetch_array($result_mukim)){
										$mukim_id = $row_mukim['mukim_id'];
										$mukim_nama = $row_mukim['mukim_nama'];
									}
									?>
									<td><?php echo $mukim_nama; ?></td>
								</tr>

							</table>
						</div>
						<div class="has-form1">
							<form method="post"  style="text-align:center" action="ketua_edit.php?id=<?php echo $row['no_kp']; ?>" >
								<input type="submit" name="save" value="Kemaskini Maklumat Ketua Keluarga" class="link btn btn-primary">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--WARIS/ISTERI-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<h2>
							B- Maklumat Isteri/Waris:
						</h2>
					</div>
					<div class="body">
						<div class="has-form">
							<?php if (!isset($row2['nama'])){ ?>
								<form method="post"  style="text-align:center" action="isteri_waris_insert.php?id=<?php echo $row['no_kp']; ?>&nama=<?php echo $row['nama']; ?>" >
									<input type="submit" name="save" value="Daftar Maklumat Isteri/Waris" class="link btn btn-primary">
								</form>
							<?php }; ?>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
																
								<!--PAPARAN WARIS/ISTERI-->
								<?php if (isset($row2['nama'])){ ?>
									<tr>
										<td width="30%">No KP</td>
										<td width="60%"><?php echo $row2['no_kp'] ?></td>
									</tr>
									
									<tr>
										<td>Nama</td>
										<td><?php echo $row2['nama'] ?></td>           
									</tr>
									
									<tr >
										<td>Tarikh Lahir</td>
										<td><?php echo $row2['tarikh_lahir'] ?></td>               
									</tr>
									
									<tr>
										<td>Jantina</td>
										<td><?php echo $row2['jantina'] ?></td>               
									</tr>
									
									<tr>
										<td>Tempat Lahir</td>
										<td><?php echo $row2['tempat_lahir'] ?></td>
									</tr>
									
									<tr>
										<td>No HP</td>
										<td><?php echo strtoupper($row2['no_hp']) ?></td>
									</tr>
									
									<tr >
										<td>Pekerjaan</td>
										<td><?php echo $row2['pekerjaan'] ?></td>
									</tr>
									
									<tr>
										<td>Pendapatan</td>
										<td>RM<?php echo $row2['pendapatan'] ?></td>
									</tr>
									
									<tr >
										<td>Tahap Pendidikan</td>
										<td><?php echo $row2['pendidikan'] ?></td>
									</tr>
								<?php	}; ?>
								
							</table>
						</div>
						
						<div class="has-form">
						<?php if (isset($row2['nama'])){ ?>
							<form method="post"  style="text-align:center" action="confirm_delete.php?id=<?php echo $row['no_kp']; ?>" >
									<input type="submit" name="save" value="Padam Maklumat Isteri/Waris" class="link btn btn-danger">
							</form>
						<?php }; ?>
						</div>
						
						<div class="has-form1">
						<?php if (isset($row2['nama'])){ ?>
							<form method="post"  style="text-align:center" action="isteri_waris_edit.php?id=<?php echo $row2['no_kp']; ?>&id_ketua=<?php echo $row['no_kp']; ?>&nama=<?php echo $row['nama']; ?>" >
								<input type="submit" name="save" value="Ubah Maklumat Isteri/Waris" class="link btn btn-primary">
							</form>
						<?php }; ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		<!--TANGGUNGAN-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<h2>
							C- Maklumat Anak Dalam Tanggungan Yang Belum Berkahwin
						</h2>
					</div>
					<div class="body">
						<div class="has-form">
							<form method="post" style="text-align:center" action="tanggungan_insert.php?id=<?php echo $row['no_kp']; ?>&nama=<?php echo $row['nama']; ?>">
								<input type="submit" name="save" value="Daftar Tanggungan" class="link btn btn-primary">
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
							
								<!--PAPARAN TANGGUNGAN-->
								
								<?php if ($count3 != 0){ ?>
								<thead>
									<tr>
										<th>No. KP</th>
										<th>Nama</th>
										<th>Tarikh Lahir</th>
										<th>Jantina</th>
										<th>Pekerjaan</th>
										<th>Pendapatan</th>
										<th>Pendidikan</th>
										<th>Pusat Pendidikan</th>
										<th>Action</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php while ($row3 = mysqli_fetch_array($result3)){ ?>
									<tr>
										<td width="15%"><?php echo $row3['no_kp']; ?></td>
										<td width="25%"><?php echo $row3['nama']; ?></td>
										<td width="10%"><?php echo $row3['tarikh_lahir']; ?></td>
										<td width="10%"><?php echo $row3['jantina']; ?></td>
										<td width="15%"><?php echo $row3['pekerjaan']; ?></td>
										<td width="15%">RM<?php echo $row3['pendapatan']; ?></td>
										<td width="15%"><?php echo $row3['pendidikan']; ?></td> 
										<td width="15%"><?php echo $row3['pusat_pendidikan']; ?></td>
										<td width="10%"><a href="tanggungan_edit.php?id=<?php echo $row3['no_kp']; ?>&id_ketua=<?php echo $row['no_kp']; ?>&nama=<?php echo $row['nama']; ?>" class="btn btn-primary">Ubah</a></td>
										<td><a href="confirm_delete_anak.php?idbapak=<?php echo $row['no_kp']; ?>&idanak=<?php echo $row3['no_kp'];?>" class="btn btn-danger">Padam</a></td>
									</tr>
								<?php $i++; } }?>
								</tbody>
							</table>
						</div>
						
						<!-- BUTANG PADAM -->	
					</div>
				</div>
			</div>
		</div>
		
		<!--LAIN MAKLUMAT-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<h2>
							D- Lain-lain Maklumat :
						</h2>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="35%">Keluasan Tanah Yang Diusahakan</td>
									<td width="65%"><?php echo $row['luas_tanah'];?></td>
								</tr>
            
								<tr>
									<td>Kilang/Kedai Yang diusahakan</td>
									<td><?php echo $row['kilang_kedai'];?></td>
								</tr>
            
								<tr>
									<td>Jumlah Kenderaan:</td>
									<td>
										Kereta: <?php echo $row['jumlah_kereta'];?><br>
										Motorsikal: <?php echo $row['jumlah_motosikal'];?><br>
										Lori: <?php echo $row['jumlah_lori'];?><br>
										Bas: <?php echo $row['jumlah_bas'];?>
									</td>
								</tr>
							</table>
						</div>
						<div class="has-form1">
							<form method="post"  style="text-align:center" action="lain_lain_maklumat_edit.php?id=<?php echo $row['no_kp']; ?>" >
									<input type="submit" name="save" value="Kemaskini Lain-Lain Maklumat" class="link btn btn-primary">
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		<!--CETAK-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="body">
						<div class="has-form">
							<form method="post"  style="text-align:center" action="read_data.php?id=<?php echo $row['no_kp']; ?>" >
									<input type="submit" name="save" value="CETAK" class="link btn btn-success"><br>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>

<?php include "../pages/template/footer.php"; ?>
