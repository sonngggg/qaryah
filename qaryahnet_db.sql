-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2019 at 02:41 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qaryahnet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `isteri_waris`
--

CREATE TABLE `isteri_waris` (
  `no_kp` varchar(14) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tarikh_lahir` date NOT NULL,
  `jantina` varchar(10) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `pendapatan` float NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `no_kp_ketua` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `isteri_waris`
--

INSERT INTO `isteri_waris` (`no_kp`, `nama`, `tarikh_lahir`, `jantina`, `tempat_lahir`, `no_hp`, `pekerjaan`, `pendapatan`, `pendidikan`, `no_kp_ketua`) VALUES
('', '', '0000-00-00', '', '', '', '', 0, '', ''),
('710204016352', 'SITI BINTI KASIM', '1971-04-02', 'PEREMPUAN', 'RUMAH', '0144563256', 'SURIRUMAH', 0, 'SPM/MCE', '710603016123'),
('720102016348', 'MAIMUNAH BINTI DERIS', '1972-01-02', 'PEREMPUAN', 'PARIT RAJA', '0123654789', 'TUKANG CUCI', 200, 'SPM/MCE', '760412016343');

-- --------------------------------------------------------

--
-- Table structure for table `ketua_keluarga`
--

CREATE TABLE `ketua_keluarga` (
  `no_kp` varchar(14) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tarikh_lahir` date NOT NULL,
  `jantina` varchar(10) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `keturunan` varchar(100) NOT NULL,
  `taraf_kahwin` varchar(15) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `emel` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `pendapatan` float NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `jumlah_anak` int(11) NOT NULL,
  `alamat_1` varchar(100) NOT NULL,
  `alamat_2` varchar(100) NOT NULL,
  `poskod` varchar(10) NOT NULL,
  `bandar` varchar(50) NOT NULL,
  `negeri` varchar(50) NOT NULL,
  `luas_tanah` varchar(50) DEFAULT NULL,
  `kilang_kedai` varchar(50) DEFAULT NULL,
  `jumlah_kereta` int(11) DEFAULT NULL,
  `jumlah_motosikal` int(11) DEFAULT NULL,
  `jumlah_lori` int(11) DEFAULT NULL,
  `jumlah_bas` int(11) DEFAULT NULL,
  `ketua_mukim` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ketua_keluarga`
--

INSERT INTO `ketua_keluarga` (`no_kp`, `nama`, `tarikh_lahir`, `jantina`, `tempat_lahir`, `keturunan`, `taraf_kahwin`, `agama`, `no_hp`, `emel`, `pekerjaan`, `pendapatan`, `pendidikan`, `jumlah_anak`, `alamat_1`, `alamat_2`, `poskod`, `bandar`, `negeri`, `luas_tanah`, `kilang_kedai`, `jumlah_kereta`, `jumlah_motosikal`, `jumlah_lori`, `jumlah_bas`, `ketua_mukim`) VALUES
('710603016123', 'HAJI DERAMAN BIN IDRIS', '1971-03-06', 'LELAKI', 'HOSPITAL', 'MELAYU', 'BERKAHWIN', 'ISLAM', '0179875641', '0', 'PERSARA', 1200, 'STPM', 2, 'NO 10', 'TIANG LAMPU 8', '86400', 'BATU PAHAT', 'JOHOR', '5 EKAR', 'WARUNG', 1, 2, 0, 0, 11),
('720125015431', 'KASIM BIN SULAIMAN', '1972-01-25', 'LELAKI', 'HOSPITAL', 'MELAYU', 'DUDA', 'ISLAM', '014856325', 'kasim@gmail.com', 'PETANI', 500, 'SPM/MCE', 2, 'NO 2 ', 'TIANG LAMPU 9', '86400', 'BATU PAHAT', 'JOHOR', '2', '0', 1, 1, 1, 0, 10),
('760412016343', 'BADRUZAMAN BIN ABDULLAH', '1976-04-12', 'LELAKI', 'BATU PAHAT', 'MELAYU', 'BUJANG', 'ISLAM', '0143245692', '0', 'BURUH', 600, 'SPM/MCE', 1, 'NO 6', 'JALAN GADING', '86400', 'BATU PAHAT', 'JOHOR', '2 EKAR', 'BENGKEL BAD', 1, 1, 0, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `mukim`
--

CREATE TABLE `mukim` (
  `mukim_id` int(3) NOT NULL,
  `mukim_nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mukim`
--

INSERT INTO `mukim` (`mukim_id`, `mukim_nama`) VALUES
(10, 'HAJI RAIS'),
(11, 'PARIT KARJO'),
(12, 'PARIT SAMIJAN'),
(13, 'PARIT JELUTONG');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `nama_pengguna` varchar(15) NOT NULL,
  `kata_laluan_pengguna` varchar(30) NOT NULL,
  `alamat_pengguna` varchar(50) NOT NULL,
  `no_tel_pengguna` varchar(15) NOT NULL,
  `jenis_pengguna` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `nama_pengguna`, `kata_laluan_pengguna`, `alamat_pengguna`, `no_tel_pengguna`, `jenis_pengguna`) VALUES
(1, 'admin', 'rahsia', 'Parit Raja', '01162852518', 'pentadbir');

-- --------------------------------------------------------

--
-- Table structure for table `tanggungan`
--

CREATE TABLE `tanggungan` (
  `no_kp` varchar(14) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tarikh_lahir` date NOT NULL,
  `jantina` varchar(10) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `pendapatan` float DEFAULT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `pusat_pendidikan` varchar(100) DEFAULT NULL,
  `no_kp_ketua` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tanggungan`
--

INSERT INTO `tanggungan` (`no_kp`, `nama`, `tarikh_lahir`, `jantina`, `pekerjaan`, `pendapatan`, `pendidikan`, `pusat_pendidikan`, `no_kp_ketua`) VALUES
('820102015648', 'HAIKAL BIN DERAMAN', '2019-10-16', 'LELAKI', 'PELAJAR', 300, 'SARJANA MUDA', 'UTHM', '710603016123'),
('920325015436', 'AISYAH BINTI KASIM', '1992-03-25', 'PEREMPUAN', 'PELAJAR', 300, 'DIPLOMA', 'UNIVERSITI MALAYA', '720125015431'),
('970715015519', 'HAIKAL BIN BADRUZAMAN', '1997-07-15', 'LELAKI', 'PELAJAR', 100, 'SARJANA MUDA', 'KKTM SRI GADING', '760412016343'),
('990203015515', 'SYAMSUL BIN KASIM', '1999-02-03', 'LELAKI', 'PELAJAR', 0, 'PT3/SRP/LCE', 'SMK SERI GADING', '720125015431');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `isteri_waris`
--
ALTER TABLE `isteri_waris`
  ADD PRIMARY KEY (`no_kp_ketua`),
  ADD UNIQUE KEY `no_kp_ketua` (`no_kp_ketua`);

--
-- Indexes for table `ketua_keluarga`
--
ALTER TABLE `ketua_keluarga`
  ADD PRIMARY KEY (`no_kp`);

--
-- Indexes for table `mukim`
--
ALTER TABLE `mukim`
  ADD PRIMARY KEY (`mukim_id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`),
  ADD UNIQUE KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `tanggungan`
--
ALTER TABLE `tanggungan`
  ADD PRIMARY KEY (`no_kp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mukim`
--
ALTER TABLE `mukim`
  MODIFY `mukim_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
