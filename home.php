<?php session_start(); ?>

<?php if(!defined("URL")) define("URL", "/aksesori_kereta/"); ?>

<?php include "../pages/template/header.php"; ?>

<?php include "../pages/template/top-bar.php"; ?>

<?php include "../pages/template/left-content.php";  ?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                
            </div>



            <div class="row clearfix">

            </div>

            <div class="row clearfix">
                <div class="panel-body">
					<div class="post">
						<div class="post-heading">
							<p></p>
						</div>
						<div class="post-content">
							<iframe width="100%" height="360" src="https://www.youtube.com/embed/JBMMgIWlW_E" frameborder="0" allowfullscreen=""></iframe>
						</div>
					</div>
                </div>

                <div class="col-xs-20 col-sm-20 col-md-18 col-lg-18">
                    <div class="card">
                        <div class="body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="../images/galerikereta2.jpg" />
                                    </div>
                                    <div class="item">
                                        <img src="../images/galerikereta.jpg" />
                                    </div>
                                    <div class="item">
                                        <img src="../images/myvi.jpg" />
                                    </div>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Example -->

                 
                <div class="col-xs-20 col-sm-20 col-md-18 col-lg-18">
                    <div class="card">
                        <div class="body">
                            <p class="lead">
								<p> <?php
									 if (!empty($_SESSION['error'])) 
									 {
									 echo $_SESSION['error'];
									 unset($_SESSION['error']);
									 }
									 ?>
								   Pengaruh dari negara-negara pengeluar kereta terkenal dunia seperti Jepun sangat kuat di kalangan pemilik kenderaan empat roda itu. Ia bukan sahaja menjadi obses orang muda malah golongan veteran. Apatah lagi kebelakangan ini kereta-kereta retro seperti Datsun, Nissan dan Toyota yang juga semakin mendapat tempat di kalangan peminat automobil. Semakin ramai mekanik berkebolehan mengubahsuai kereta-kereta 'tua' menjadi 'muda' semula, selain disokong industri alat ganti alternatif yang murah. Satu aspek yang kini dititikberatkan oleh peminat kereta ialah kitkenderaan atau bodykit. Dalam dunia automotif, industri ini mula berkembang pesat pada akhir tahun 1990-an.
								</p>
								<p>
									Bodykit bukan sahaja didapati secara meluas di pasaran selepas jualan atau kedai aksesori, malah ia juga kini menjadi pilihan industri sebagai keperluan pengeluar kelengkapan asal (OEM). Kit kenderaan ialah koleksi modifikasi luaran kereta yang biasanya melibatkan komponen seperti spoiler bumbung belakang, skirting sisi dan lips (ada yang menggelarnya spoiler) depan serta belakang. Ini ditambah komponen lain seperti diffuser, skup bonet dan lain-lain. Kebiasaannya kit seperti itu dipadankan dengan rekaan secara teliti dari depan sehingga belakang kenderaan supaya ia aerodinamik, terkini, menarik, tidak selekeh dan sepadan. Satu ketika dahulu bodykit OEM amat jarang ditemui setiap kali kita membeli kereta baru tetapi trend masa kini adalah sebaliknya. Dengan pelbagai reka bentuk, ada pengeluar menggunakan tuner bebas yang tersohor termasuk Impul, AMG, Wald dan Brabus.
									Di Malaysia terdapat beberapa syarikat yang menghasilkan kit kenderaan sama ada OEM atau selepas jualan. Antaranya Air Master Trading Sdn. Bhd. (Air Master) dengan kilang pengeluarannya di Sri Damansara, Kuala Lumpur. Menurut Pengurus Perniagaan Air Master, Hong Soo Hoe, bodykit bukan sesuatu yang diada-adakan untuk tujuan kosmetik tetapi ia banyak melibatkan fakta saintifik yang dapat membantu kelancaran kereta terutama ketika memecut pada kelajuan tinggi. Katanya, setiap kenderaan dibina dengan bahagian bawahnya berbeza dikenali sebagai kesan permukaan iaitu bahagian yang dibina bagi merapatkan ruang antara lantai dengan permukaan tanah.

								</p>
							</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>  <!-- #END# Task Info -->
    </section>

<?php include "../pages/template/footer.php"; ?>
</body>

</html>
